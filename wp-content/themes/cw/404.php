<?php
/**
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
get_header(); ?>

	<div class="main row" role="main">
		<div class="small-12 medium-12 large-12 columns page_inner page_title_bg">
			<h2 class="entry-title"><?php _e( 'This is somewhat embarrassing, isn&rsquo;t it?' ); ?></h2>
			<div class="small-12 medium-12 large-8 large-push-4 columns page_inner">
				<p><?php _e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.' ); ?></p>
			</div>
		</div>

		<?php get_sidebar(); ?>
	</div>

<?php get_footer(); ?>