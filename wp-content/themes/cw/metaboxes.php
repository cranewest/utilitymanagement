<?php
// ------------------------------------
//
// Custom Meta Boxes
//
// ------------------------------------
// custom meta boxes
function list_pages() {
	$pages_args = array(
		'sort_order' => 'ASC',
		'sort_column' => 'post_title'
		);

	$pages = get_pages($pages_args);

	global $cw_page_list;
	$cw_page_list = array('' => 'Select a page');
	foreach($pages as $page) {
		$page_name = $page->post_title;
		$page_id = $page->ID;
		$cw_page_list[$page_id] = $page_name;
	}
}
list_pages();

function cw_list_media() {
	$margs = array(
		'post_type' => 'attachment',
		'post_mime_type' =>'application/pdf, application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
		'post_status' => 'inherit',
		'posts_per_page' => -1
	);

	$media = new WP_Query( $margs );

	global $cw_media_list;
	$cw_media_list = array('' => 'Select a file');
	foreach($media->posts as $file) {
		$cw_media_list[$file->ID] = $file->post_title;
	}
}
cw_list_media();

function cw_metaboxes( array $meta_boxes ) {
	global $cw_page_list;
	// use for select, checkbox, radio of list of states
	global $cw_states;

	$prefix = '_cwmb_'; // Prefix for all fields

	$meta_boxes['slide_video'] = array(
		'id' => 'slide_video',
		'title' => 'Slide Video Files',
		'object_types' => array( 'slides' ), // Post type
		'context' => 'normal',
		'priority' => 'high',
		'show_names' => true, // Show field names on the left
		'fields' => array(
			array(
				'name' => 'Video mp4',
				'desc' => 'NOTE: Uploading video will override any other content. Both video codec must be uploaded for video to play.',
				'id' => $prefix.'slide_mp4',
				'type' => 'file',
			),
			array(
				'name' => 'Video webm',
				'desc' => 'NOTE: Uploading video will override any other content. Both video codec must be uploaded for video to play.',
				'id' => $prefix.'slide_webm',
				'type' => 'file',
			)
		)
	);

	$meta_boxes['slides'] = array(
		'id' => 'slides',
		'title' => 'Slideshow Info',
		'object_types' => array( 'slides' ), // Post type
		'context' => 'normal',
		'priority' => 'high',
		'show_names' => true, // Show field names on the left
		'fields' => array(
			array(
				'name' => 'Title',
				'id' => $prefix.'slide_title',
				'type' => 'text',
			),
			array(
				'name' => 'Subtitle',
				'id' => $prefix.'slide_subtitle',
				'type' => 'text',
			),
			array(
				'name' => 'Title Position',
				'id' => $prefix.'slide_title_pos',
				'type' => 'radio',
				'options' => array(
					'left' => 'Left',
					'center' => 'Center',
					'right' => 'Right'
				),
				'default' => 'left'
			),
			array(
				'name' => 'Image',
				'id' => $prefix.'slide_image',
				'type' => 'file'
			),
			array(
				'name' => 'Image Position',
				'id' => $prefix.'slide_image_pos',
				'type' => 'radio',
				'options' => array(
					'left' => 'Left',
					'center' => 'Center',
					'right' => 'Right'
				),
				'default' => 'center'
			),
			array(
				'name' => 'Caption',
				'id' => $prefix.'slide_caption',
				'type' => 'textarea'
			),
			array(
				'name' => 'Caption Position',
				'id' => $prefix.'slide_caption_pos',
				'type' => 'radio',
				'options' => array(
					'left' => 'Left',
					'center' => 'Center',
					'right' => 'Right'
				),
				'default' => 'left'
			),
			array(
				'name' => 'Link',
				'id' => $prefix.'slide_link',
				'type' => 'text_url'
			),
			array(
				'name' => 'Background Color',
				'id' => $prefix.'slide_color',
				'type' => 'radio',
				'options' => array (
					'#ffffff' => 'None',
					'#0075bf' => 'Blue',
					'#ff685a' => 'Salmon',
					'#5b5b5b' => 'Grey',
					'#4a9231' => 'Green'
				),
				'default' => '#0075bf'
			)
		)
	);

	$meta_boxes['slide_page'] = array(
		'id' => 'slide_page',
		'title' => 'Select Page(s)',
		'object_types' => array( 'slides' ), // Post type
		'context' => 'side',
		'priority' => 'high',
		'show_names' => true, // Show field names on the left
		'fields' => array(
			array(
				'name' => '',
				'desc' => 'Choose page(s) to show this slide on. NOTE: If no page is selected, this slide will note be shown anywhere on the site.',
				'id' => $prefix.'slide_page',
				'type' => 'select',
				'options' => $cw_page_list
			)
		)
	);

	$meta_boxes['locations'] = array(
		'id' => 'locations',
		'title' => 'Location Info',
		'object_types' => array('locations'), // post type
		'context' => 'normal',
		'priority' => 'default',
		'show_names' => true, // Show field names on the left
		'fields' => array(
			array(
				'name' => 'Address 1',
				'id' => $prefix.'loc_address1',
				'type' => 'text'
			),
			array(
				'name' => 'Address 2',
				'id' => $prefix.'loc_address2',
				'type' => 'text'
			),
			array(
				'name' => 'City',
				'id' => $prefix.'loc_city',
				'type' => 'text'
			),
			array(
				'name' => 'State',
				'id' => $prefix.'loc_state',
				'type' => 'select',
				'options' => $cw_states
			),
			array(
				'name' => 'Zip',
				'id' => $prefix.'loc_zip',
				'type' => 'text'
			),
			array(
				'name' => 'Phone',
				'id' => $prefix.'loc_phone',
				'type' => 'text'
			),
			array(
				'name' => 'Phone 2',
				'id' => $prefix.'loc_phone2',
				'type' => 'text'
			),
			array(
				'name' => 'Fax',
				'id' => $prefix.'loc_fax',
				'type' => 'text'
			),
			array(
				'name' => 'Email',
				'id' => $prefix.'loc_email',
				'type' => 'text'
			),
			array(
				'name' => 'Photo',
				'id' => $prefix.'loc_photo',
				'type' => 'file'
			),
			array(
				'name' => 'Hours',
				'id' => $prefix.'loc_hours',
				'type' => 'textarea'
			),
			array(
				'name' => 'Lat',
				'desc' => 'In the case that Google maps show the inccorrect location, use lat & lon',
				'id' => $prefix.'loc_lat',
				'type' => 'text'
			),
			array(
				'name' => 'Lon',
				'desc' => 'In the case that Google maps show the inccorrect location, use lat & lon',
				'id' => $prefix.'loc_lon',
				'type' => 'text'
			)
		)
	);

	// promos
	$meta_boxes['promos'] = array(
		'id' => 'promos',
		'title' => 'Promo Info',
		'object_types' => array('promos'), // post type
		// 'show_on' => array( 'key' => 'page-template', 'value' => 'template-contact.php' ), // Limit to page template :)
		'context' => 'normal',
		'priority' => 'default',
		'show_names' => true, // Show field names on the left
		'fields' => array(
			array(
				'name' => 'Promo Image',
				'id' => $prefix.'promo_image',
				'type' => 'file'
			),
			array(
				'name' => 'Link',
				'id' => $prefix.'promo_link',
				'type' => 'text_url'
			)
		)
	);

	// services
	$meta_boxes['services'] = array(
		'id' => 'services',
		'title' => 'Options',
		'object_types' => array('services'), // post type
		// 'show_on' => array( 'key' => 'page-template', 'value' => 'template-contact.php' ), // Limit to page template :)
		'context' => 'side',
		'priority' => 'default',
		'show_names' => true, // Show field names on the left
		'fields' => array(
			// array (
			// 	'name' => 'Service Image',
			// 	'id' => $prefix.'service_image',
			// 	'type' => 'file'
			// ),
			array(
				'name' => 'Link',
				'id' => $prefix.'link',
				'desc' => 'Include (http://)',
				'type' => 'text'
			)
		)
	);

	$meta_boxes['testimonials'] = array(
		'id' => 'testimonials',
		'title' => 'Testimonial',
		'object_types' => array('testimonials'), // post type
		// 'show_on' => array( 'key' => 'page-template', 'value' => 'template-contact.php' ), // Limit to page template :)
		'context' => 'normal',
		'priority' => 'default',
		'show_names' => true, // Show field names on the left
		'fields' => array(
			array(
				'id' => $prefix.'testimonial',
				'type' => 'textarea'
			),
			array(
				'name' => 'Vocation',
				'id' => $prefix.'vocation',
				'type' => 'text'
			),
			array(
				'name' => 'Location',
				'id' => $prefix.'location',
				'type' => 'text'
			)
		)
	);

	$meta_boxes['staff'] = array(
		'id' => 'staff',
		'title' => 'Team Member Info',
		'object_types' => array('staff'), // post type
		// 'show_on' => array( 'key' => 'page-template', 'value' => 'template-contact.php' ), // Limit to page template :)
		'context' => 'normal',
		'priority' => 'default',
		'show_names' => true, // Show field names on the left
		'fields' => array(
			array(
				'name' => 'Title',
				'id' => $prefix.'staff_title',
				'type' => 'text'
			),
			array(
				'name' => 'Image',
				'id' => $prefix.'staff_image',
				'type' => 'file'
			),
			array(
				'name' => 'Bio',
				'id' => $prefix.'staff_bio',
				'type' => 'textarea'
			)
		)
	);

	// example of repeating metaboxes for page template
	// $meta_boxes['careers'] = array(
	// 	'id' => 'careers',
	// 	'title' => 'Career Opportunities',
	// 	'object_types' => array('page'), // post type
	// 	// 'show_on' => array( 'key' => 'page-template', 'value' => 'page-templates/page-template-file-here.php' ), // Limit to page template
	// 	'context' => 'normal',
	// 	'priority' => 'default',
	// 	'show_names' => true, // Show field names on the left
	// 	'fields' => array(
	// 		array (
	// 			'id' => $prefix . 'careers',
	// 			'type' => 'group',
	// 			'description' => '',
	// 			'options' => array(
	// 				'group_title' => 'Tab {#}', // since version 1.1.4, {#} gets replaced by row number
	// 				'add_button' => 'Add another item',
	// 				'remove_button' => 'Remove item',
	// 				'sortable' => true, // beta
	// 			),
	// 			// Fields array works the same, except id's only need to be unique for this group. Prefix is not needed.
	// 			'fields' => array(
	// 				array(
	// 					'name' => 'Career Title',
	// 					'id' => 'career_title',
	// 					'type' => 'text',
	// 				),
	// 				array(
	// 					'name' => 'Career Content',
	// 				'id' => 'career_content',
	// 				'type' => 'wysiwyg',
	// 				)
	// 			)
	// 		)
	// 	)
	// );

	//news link
	$meta_boxes['news_link1'] = array(
		'id' => 'news_link1',
		'title' => 'Article Link',
		'object_types' => array('post'), // post type
		'priority' => 'default',
		'show_names' => true, // Show field names on the left
		'fields' => array(
			array(
				'name' => 'Link',
				'id' => $prefix.'link',
				'desc' => 'Include (http://)',
				'type' => 'text'
			)
		),
	);

	return $meta_boxes;
}
add_filter( 'cmb2_meta_boxes', 'cw_metaboxes' );

// end custom meta boxes