<?php
/**
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
?>
<div class="footer_area">
	<footer role="contentinfo" class="row">
		<div class="small-12 medium-8 large-8 columns">
			<h6 class="partners"><strong>Our Partners</strong></h6>
			<h4 class="partners_images">
				<div class="small-9 medium-10 large-10 columns"><img src="<?php bloginfo('template_directory'); ?>/img/img/partners3.gif" ></div>
				<div class="small-3 medium-2 large-2 columns"><a href="/energy-star/"><img src="<?php bloginfo('template_directory'); ?>/img/img/partners_e.gif" ></a></div>
			</h4>
			
		</div>
		<div class="small-12 medium-4 large-4 columns">
			<div class="f_right footer_c">
				<p class="">&copy; <?php echo date('Y'); ?> <?php bloginfo( 'name' ); ?></p>
				<!-- <a class="cw" href="http://crane-west.com/"><img src="<?php echo get_template_directory_uri(); ?>/img/cw-dark.png" alt="Site Powered by Crane | West" /></a> -->
				<a href="/privacy-policy/">Privacy Policy</a>
				|
				<a href="/sitemap">Sitemap</a>
			</div>
		</div>
	</footer>
</div>

	<!-- WP_FOOTER() -->
	<?php wp_footer(); ?>
	<?php
		// must activate CW Options Page plugin for the line below to work
		$ga_code = cw_options_get_option('cwo_ga'); if( !empty($ga_code) ) {
	?>
	<script type="text/javascript">
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', '<?php echo $ga_code; ?>', 'auto');
		ga('send', 'pageview');
	</script>
	<?php } ?>
</body>
</html>