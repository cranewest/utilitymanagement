<?php
/* Template Name: UMC Markers Page*/
?>

<?php
/**
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
get_header(); ?>

	<div class="main row" role="main">
		<div class="small-12 medium-12 large-12 columns page_inner page_title_bg">
			<?echo '<h5 class="page-title">'.get_the_title().'</h5>';?>
		</div>


		<div class="small-12 medium-12 large-8 large-push-4 columns page_inner">
			<?php
				if(have_posts()) {
					while(have_posts()) {
						the_post();
						// echo '<h3 class="page-title">'.get_the_title().'</h3>';
						echo '<div class="page_content">';
							the_content();
						echo '</div>';
						// comments_template( '', true );
					}
				}
			?>

			<?php
				if ( wp_is_mobile() ) {
					?>
						<div >
							<img src="<?php bloginfo('template_directory'); ?>/img/img/umc_map1.1.png">
							<img src="<?php bloginfo('template_directory'); ?>/img/img/umc_map2.1.png">
						</div>
					<?
				}
				else{

					?>
						<div class="umc_map">
							<object width="100%" data="<?php bloginfo('template_directory'); ?>/img/flash/maps.swf"></object>
						</div>
					<?

				}
			?>
			

		</div>

		<aside class="widget-area small-12 medium-12 large-4 large-pull-8 columns umc_sidebar" role="complementary ">
			<div class="slidebar_inner">
				
				<h3 class="widget-title">Market Segments</h3><div class="menu-services-container">
				<ul id="menu-services" class="menu">
					<li  class="">Healthcare</li>
					<li  class="">Education</li>
					<li  class="">Municipalities</li>
					<li  class="">Hospitality</li>
					<li  class="">Multi-Family</li>
					<li  class="">Restaurants</li>
					<li  class="">Grocery Chains</li>
					<li  class="">Industrial</li>
					<li  class="">Manufacturing</li>
					<li  class="">Roofing</li>
					<li  class="">Commercial Real Estate</li>
					<li  class="">Banking</li>
				</ul></div>

			</div>
		</aside>

		
	</div>

<?php get_footer(); ?>
