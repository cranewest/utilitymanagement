<?php
/* Template Name: Green Solutions Page*/
?>
<div class="green_page_bg">
<?php
/**
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
get_header(); ?>

	<div class="main row" role="main">
		<div class="small-12 medium-12 large-12 columns page_inner page_title_bg_green">
			<?echo '<h5 class="page-title">'.get_the_title().'</h5>';?>
		</div>
		
		

		<div class="small-12 medium-12 large-8 large-push-4 columns page_inner">
			<?php
				if(have_posts()) {
					while(have_posts()) {
						the_post();
						// echo '<h3 class="page-title">'.get_the_title().'</h3>';
						echo '<div class="page_content">';
							the_content();
						echo '</div>';
						// comments_template( '', true );
					}
				}
			?>

		</div>
		<?php get_sidebar(); ?>
		
	</div>

<?php get_footer(); ?>
</div>