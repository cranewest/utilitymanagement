<?php
/* Template Name: News Page*/
?>
<?php
/**
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
get_header(); ?>

	<div class="main row" role="main">
		<div class="small-12 medium-12 large-12 columns page_inner page_title_bg">
			<?echo '<h5 class="page-title">'.get_the_title().'</h5>';?>
		</div>
		
		<div class="small-12 medium-12 large-8 large-push-4 columns page_inner umc_news">
			<?php 	
				//adjusting the query
				$args = array(
					// 'category_name' => 'news',
					'post_type' => 'post',
					'posts_per_page' => -1,
					'order' => DESC
				);

				// The Query
				$latest_post = new WP_Query( $args );
				// The Loop
				if ( $latest_post->have_posts() ) 
				{
					while ( $latest_post->have_posts() ) 
					{	
						$latest_post->the_post();
						
						echo'<div class = "">';
							$n_link = get_post_meta($post->ID, '_cwmb_link', true);
							if($n_link)
							{
								?><h6 class="h_grey"><a href="<? echo $n_link; ?>"><? echo get_the_title(); ?></a></h6><?
							}
							else{
								?><h6 class="h_grey"><a href="<? the_permalink(); ?>"><? echo get_the_title(); ?></a></h6><?
							}
							//echo'<div class="grey_bg_title columns small-12 medium-3 large-3"><h6 class="news_date">Testimonials</h6></div>';
							//echo'<div class="columns small-12 medium-3 large-3"><h6 class="news_date">'. get_the_date(). '</h6><br></div>';
			
							echo'<div class="act_content">';
								the_excerpt();
							echo'</div>';

							if($n_link)
							{
								?><a class=""href="<?php echo $n_link; ?>">Read More</a><?
							}
							else{
								?><a class=""href="<?php the_permalink(); ?>">Read More</a><?
							}
						echo'</div><hr>';
						
					}
				} 
				else 
				{
					// no posts do nothing
				}

				/* Restore original Post Data */
				wp_reset_postdata();
			?>
		</div>
		<?php get_sidebar(); ?>

		
	</div>

<?php get_footer(); ?>