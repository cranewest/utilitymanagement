<?php
/* Template Name: Movie Page*/
?>
<?php
/**
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
get_header(); ?>
	<div class="slide_area">
		<div class="row slide_inner">
			<!-- <img class="title_bg" src="<?php bloginfo('template_directory'); ?>/img/img/home_logo.jpg"  height="192" > -->
			<div class="small-12 medium-12 large-4 columns slide_inner">

			</div>
			<div class="small-12 medium-12 large-8 columns slideshow_padding">
				<?php get_template_part('content', 'slides'); ?>
			</div>
		</div>
	</div>

	<div class="slide_bot_border"></div>
	<div class="main row home_content" role="main ">
		<div class="small-12 medium-4 large-4 columns">
			<div class="home_inner_sidebar_padding">
			<h6 class="partners"><strong>News</strong></h6><br>
			<?php 	
				//adjusting the query
				$args = array(
					// 'category_name' => 'news',
					'post_type' => 'post',
					'posts_per_page' => 4,
					'order' => DESC
				);

				// The Query
				$latest_post = new WP_Query( $args );
				// The Loop
				if ( $latest_post->have_posts() ) 
				{
					while ( $latest_post->have_posts() ) 
					{	
						$latest_post->the_post();
						
						echo'<div class = "">';
							$n_link = get_post_meta($post->ID, '_cwmb_link', true);
							if($n_link)
							{
								?><li class="h_grey"><a target="_blank" href="<? echo $n_link; ?>"><? echo get_the_title(); ?></a></li><?
							}
							else{
								?><li class="h_grey"><a href="<? the_permalink(); ?>"><? echo get_the_title(); ?></a></li><?
							}
							
						echo'</div><br>';
						
					}
				} 
				else 
				{
					// no posts do nothing
				}

				/* Restore original Post Data */
				wp_reset_postdata();
			?>

			<a class=""href="<?php echo get_site_url(); ?>/news/">Read All News</a><br><br><br>

			<a href="/reo-solutions/"><img src="<?php bloginfo('template_directory'); ?>/img/img/b-reo.jpg"></a><br>
			<a target="_blank" href="http://www.cmegroup.com/trading/energy/natural-gas/natural-gas.html"><img src="<?php bloginfo('template_directory'); ?>/img/img/3_oil.gif"></a><br>
			<a target="_blank" href="http://www.cmegroup.com/trading/energy/crude-oil/light-sweet-crude.html"><img src="<?php bloginfo('template_directory'); ?>/img/img/3_gas.gif"></a><br>
			</div>
		</div>

		<div class="small-12 medium-8 large-8 columns ">
			<div class="small-12 medium-8 large-8 columns">
				<div class="m_padding">
				<?php
					if(have_posts()) {
						while(have_posts()) {
							the_post();
							// echo '<h2 class="page-title">'.get_the_title().'</h2>';
							the_content();
							// comments_template( '', true );
						}
					}
				?>
				</div>
				
			</div>
			<div class="umc_vid">
				<video width="100%" controls>
				  <source src="<?php bloginfo('template_directory'); ?>/img/vid/um_present.mp4" type="video/mp4">
				  <!-- <source src="<?php bloginfo('template_directory'); ?>/img/vid/umc_wb.ogv" type="video/ogv"> -->
				  <!-- <source src="<?php bloginfo('template_directory'); ?>/img/vid/umc_wb.webm" type="video/webm"> -->

				</video>
				<object width="100%"  data="<?php bloginfo('template_directory'); ?>/img/flash/umc_wb.swf"></object>
			</div>
		</div>

		<?php //get_sidebar(); ?>
	</div>

<?php get_footer(); ?>