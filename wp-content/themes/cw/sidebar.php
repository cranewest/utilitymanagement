<?php
/**
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
?>
	<aside class="widget-area small-12 medium-12 large-4 large-pull-8 columns umc_sidebar" role="complementary ">
		<!-- <div class="slide_title_image">

		</div> -->
		<!-- <img src="<?php bloginfo('template_directory'); ?>/img/img/inside_Logo1.jpg"  height="100" width="400"> -->
		<div class="slidebar_inner">
			<?php //dynamic_sidebar( 'sidebar-1' ); ?>
			<h3 class="widget-title">Services</h3><br>
			<?php 	
				//adjusting the query
				$args = array(
					// 'category_name' => 'news',
					'post_type' => 'services',
					'posts_per_page' => -1,
					'order' => DESC
				);

				// The Query
				$latest_post = new WP_Query( $args );
				// The Loop
				if ( $latest_post->have_posts() ) 
				{
					while ( $latest_post->have_posts() ) 
					{	
						$latest_post->the_post();
						
						echo'<div class = "">';
							$n_link = get_post_meta($post->ID, '_cwmb_link', true);
							if($n_link)
							{
								?><li ><a target="_blank" href="<? echo $n_link; ?>"><? echo get_the_title(); ?></a></li><?
							}
							else{
								?><li ><a href="<? the_permalink(); ?>"><? echo get_the_title(); ?></a></li><?
							}
							
						echo'</div><br>';
						
					}
				} 
				else 
				{
					// no posts do nothing
				}

				/* Restore original Post Data */
				wp_reset_postdata();
			?>
		</div>


	</aside>